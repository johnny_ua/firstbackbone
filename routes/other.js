/**
 * Created by Ivan on 24.05.2015.
 */
var ShadowDragon = require('../modules/shadowDragon');
var RedDragon = require('../modules/redDragon');
var Map = require('../modules/map.js');

var ways= require('../modules/ways');
var express = require('express');
var Router = express.Router();
var mongoose = require('mongoose');
var config = require('../config/index.js');
var HeroSchema = mongoose.Schemas['Hero'];
var MapSchema = mongoose.Schemas['Map'];

module.exports = function (db) {
    var HeroModel = db.model('hero', HeroSchema);
    var MapModel = db.model('map', MapSchema);

    var createHero = function (map, obj){
        if(obj.clan == 'red')return new RedDragon(map, obj.name, obj.type, obj.hp, obj.pw, obj.position, obj.way);
        else return new ShadowDragon(map,obj.name, obj.type, obj.hp, obj.pw, obj.position, obj.way);
    };

    Router.delete('/dropDataBase', function (req, res, next) {
        var secret=req.body.secret;
        if(secret==config.secret){
            HeroModel.remove({}, function(err) {
                if(err){
                    return next(err);
                }
                console.log('Heroes collection removed')
            });
            MapModel.remove({}, function(err) {
                if(err){
                    return next(err);
                }
                console.log('Maps collection removed')
            });
           return res.status(200).send('Collections removed. You can start new Game');
        }
        else return res.status(403).send('Your password is wrong');
    });



    Router.get('/:mapName/autoPlay/:timeout', function (req, res, next) {
        MapModel.findOne({name: req.params.mapName})
            .populate('heroes')
            .exec(function (err, resMap) {
                if(err) return next(err);
                var map = new Map(resMap.name);
                for(var i=0, count = resMap.heroes.length; i<count ;i++){
                    HeroModel.findById(resMap.heroes[i].id ,function(err, result) {
                        if (err) return next(err);
                        if (result.currentMap != resMap.id)return next(err);
                        map.heroes.push(createHero(map, result));
                    });
                }
                //ToDo: доробити автогру
                res.status(200).send('Game is started, turn to console');
                var count=1;
                var has;
                var timeout=req.params.timeout;
                var play=true;
                map.description();
                while (!map.gameOver()&&play) {
                    has=false;
                    for(var i = 0,l = map.heroes.length; i < l;i++)
                        if(!map.heroes[i].way.length==0)play=true;
                    console.log('************!!! ROUND '+ count + ' !!!************')
                    map.updateEnv();
                    count++;
                    map.timer(timeout, function () {
                        map.printAllHeroes();
                        map.autoPlay();
                    });
                }
                map.printAllHeroes();
                console.log(map.gameOver());


            });
    });

    return Router;
};
