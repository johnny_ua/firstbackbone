var Map = require('../modules/map.js');

var express = require('express');
var Router = express.Router();

var mongoose = require('mongoose');
var MapSchema = mongoose.Schemas['Map'];



    //****************************************
    global.map={};
    global.map=map;
    process.env.NODE_ENV='development';
    //****************************************
module.exports = function (db) {
    var MapModel = db.model('map', MapSchema);

var upHistory= function(mapName, message){
        var log = {
            createdAt: Date.now(),
            log: message
        };
        //console.log(log);
    MapModel.findOne({name: mapName}).exec(function(err, result) {
        if(err) throw (err);
        if(!result)throw (new Error('Cant find history'));
            result.history.addToSet(log);
        result.save(function (err) {
            if (err) throw (err);
        });
    });
};

//======================================================================================================================
    Router.get('/', function (req, res, next) {
        MapModel.find({}, '_id name maxWidth maxHeight heroes')
            .populate({path:'heroes', select: 'name type clan hp pw _id'})
        .exec(function (err, maps) {
                if(err) return next(err);

                res.status(200).send(maps);
            });
    });
 /*   Router.get('/', function(req, res, next){
        MapModel.find(function(err, maps){
            if(err){
                return next(err);
            }
            res.status(200).send(maps);
        });
    });*/
//======================================================================================================================
//створити мапу

    Router.post('/', function (req, res, next) {
        console.log(data);
        var data = req.body;
        if(!data) {
            return next(err);
        }
        var map = new MapModel(data);
        map.save(function (err, result) {
            if(err){
                return next(err);
            }
            upHistory(data.name, 'Map Created');
            res.status(200).send('Map created: \n'+result.description);
        });
    });
//======================================================================================================================
//отримати інформацію про всі карти

    Router.get('/game/info', function (req, res, next) {
        MapModel.find({},'-_id name about author heroes')
            .populate({path:'heroes', select: 'name type clan hp pw -_id'})
            .exec(function (err, maps) {
                if(err) return next(err);

                res.status(200).send(maps);
            });
    });

//======================================================================================================================
//отримати інформацію про поточну карту

    Router.get('/:mapName/info', function (req, res, next) {
        var mapName=req.params.mapName;
        MapModel.findOne({name: mapName},'-_id name about author heroes')
            .populate({path:'heroes', select: 'name type clan hp pw -_id'})
            .exec(function (err, map) {
                if(err) return next(err);
                if(!map) return res.status(404).send('Map is not found')
                upHistory(mapName, 'Getting info');
                res.status(200).send(map);
            });
    });

//======================================================================================================================
//редагувати карту

    Router.put('/map/:map', function (req, res, next) {
        var mapName= req.params.map;

        var body = req.body;
        MapModel.findOne({name: mapName}).exec(function (err, map) {
            if(err) return next(err);
            if(!map)return next('There is no such map');

            var insObj={
                _id: map.id,
                heroes: map.heroes,
                wind: body.wind || map.wind,
                name: body.name || map.name,
                maxWidth: body.maxWidth || map.maxWidth,
                maxHeight: body.maxHeight || map.maxHeight,
                resistance: body.resistance || map.resistance,
                gravity: body.gravity || map.gravity,
                hasObstacles: body.hasObstacles || map.hasObstacles,
                hasSnow: body.hasSnow || map.hasSnow,
                hasWater: body.hasWater || map.hasWater,
                hasPit: body.hasPit || map.hasPit,
                minHeroes: body.minHeroes || map.minHeroes,
                about: body.about || map.about,
                author: body.author || map.author
            };
            var insMap = new MapModel(insObj);

            MapModel.findByIdAndUpdate(map.id,{$set: insMap }, function(err, map){
                if (err) next(err);
                var mes= mapName + ' is changed. \nChanged map: \n'+insMap;

                upHistory(insMap.name, 'Map is changed');
                res.status(200).send(mes);
                });
            });
        });

//======================================================================================================================
    //error Handler

    Router.use(
        function (err, req, res, next) {
            var status = err.status || 500;
            var message;
            if (process.env.NODE_ENV === 'development') {
                message = err.message + '\n\r ' + err.stack;
            } else {
                message = err.message;
            }

            res.status(status).send(message);
            console.log();
        });

return Router;
};
