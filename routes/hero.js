
var express = require('express');
var heroRouter = express.Router();

var ShadowDragon = require('../modules/shadowDragon');
var RedDragon = require('../modules/redDragon');
var Map = require('../modules/map');
var Vec2= require('../modules/vec2');

var mongoose = require('mongoose');
var HeroSchema = mongoose.Schemas['Hero'];
var MapSchema = mongoose.Schemas['Map'];

module.exports = function (db) {
    var HeroModel = db.model('hero', HeroSchema);
    var MapModel = db.model('map', MapSchema);

    var createHero = function (map, obj){
        if(obj.clan == 'red')return new RedDragon(map, obj.name, obj.type, obj.hp, obj.pw, obj.position, obj.way);
        else return new ShadowDragon(map,obj.name, obj.type, obj.hp, obj.pw, obj.position, obj.way);
    };
    var upHistory= function(mapName, message){
        var log = {
            createdAt: Date.now(),
            log: message
        };
        //console.log(log);
        MapModel.findOne({name: mapName}).exec(function(err, result) {
            if(err) throw (err);
            if(!result)throw (new Error('Cant find history'));
            result.history.addToSet(log);
            result.save(function (err) {
                if (err) throw (err);
            });
        });
    };
//======================================================================================================================
    heroRouter.get('/', function (req, res, next) {
        var id = req.params.id;
        HeroModel.find({}, '_id name type clan position currentMap')
            .populate({path:'currentMap', select: ' _id  name'})
            .exec(function (err, heroes) {
                if(err) return next(err);
                res.status(200).send(heroes);
            });
    });
//======================================================================================================================
//створення героя

heroRouter.post('/', function (req, res, next) {
        //var mapName= req.params.map;
        var body = req.body;
        var newHero = new HeroModel(body);

            MapModel.findOneAndUpdate({_id: body.currentMap}, {$addToSet:{"heroes": newHero._id}}).exec(function (err, map) {
                if(err) return next(err);
                if(!map) return  res.status(400).send('At first created the map!');

            });
            newHero.save(function (err, hero) {
                if (err) {
                    return next(err);
                }
                upHistory(mapName, 'Hero '+hero.name+' created');
                res.status(200).send('Hero created:  \n'+ hero.description+'\n current map ');
            });
        });

//======================================================================================================================
//видалення героя

    heroRouter.delete('/:id', function (req, res, next) {
        var id= req.params.id;
        var heroName = req.params.heroName;
                HeroModel.findOneAndRemove({_id:id}, function(err, hero) {
                    if(err) return next(err);
                    if(!hero)return  res.status(404).send('Hero is not found, please check the name');
                    MapModel.findOne( { _id: hero.currentMap }, function(err, result){
                        if(err) return next(err);
                        if(!result)return  res.status(404).send('Map is not found, please check the name');

                        result.heroes.pull({_id:hero.id});
                        result.save(function (err) {
                            if (err) return next(err);
                            upHistory(result.name, 'Hero '+hero.name+' removed');
                            res.status(200).send('Hero removed:  \n'+ hero.description+'\n Hero removed from map: '+ result.name);
                        });
                    });
                });
            });

//======================================================================================================================
//виведення інформації

    heroRouter.get('/:heroName', function (req, res, next) {
        var heroName = req.params.heroName;
            HeroModel.findOne({name:heroName},'-_id')
                .populate({path:'currentMap', select: '-_id -heroes'})
                .exec(function (err, hero) {
                    if(err) return next(err);
                    if(!hero)return  res.status(404).send('Hero is not found, please check the name');
                    res.status(200).send(hero);
                });
            });


//======================================================================================================================
//редагування героя

heroRouter.put('/:map/:heroName', function (req, res, next) {
        var heroName= req.params.heroName;

        var body = req.body;
            HeroModel.findOne({name: heroName}).exec(function (err, hero) {
                if(err) return next(err);
                if(!hero)return next('There is no such hero');

                var insObj={
                    _id: hero.id,
                    clan: hero.clan, //ну по-суті не варто міняти
                    currentMap: hero.currentMap,
                    name: body.name || hero.name,
                    type: body.type || hero.type,
                    hp: body.hp || hero.hp,
                    pw: body.pw || hero.pw,
                    hasCounterAttack: body.hasCounterAttack || hero.hasCounterAttack,
                    speed: body.speed || hero.speed,
                    rangeMove: body.rangeMove || hero.rangeMove,
                    rangeStrike: body.rangeStrike || hero.rangeStrike,
                    way: body.way || hero.way,
                    position: body.position|| hero.position
                };
                var insHero = new HeroModel(insObj);
                HeroModel.findByIdAndUpdate(hero.id,{$set: insHero }, function(err, hero){
                    if (err) next(err);
                    upHistory(req.params.map, hero.name + ' is changed.');
                    res.status(200).send(hero.name + ' is changed. \nChanged hero: \n'+insHero);
                });
            });
    });
//======================================================================================================================
//moveTo якщо точка в межах доступного руху

    heroRouter.post('/:mapName/:heroName/moveTo', function (req, res, next) {
        var body= req.body;
        var mapName= req.params.mapName;
        var heroName=req.params.heroName;
       var hero;

        //знаходжу карту
        MapModel.findOne({name: mapName}).exec(function (err, resMap) {
            if(err)return next(err);
            if(!resMap)return res.status(404).send('Map is not found, please check the name');

            //знаходжу героя і переверіяю чи він на цій карті
            HeroModel.findOne({ name: heroName},function(err, result){
            if(err) return next(err);
                if(result.currentMap!=resMap.id)return next(err);

               var map=new Map(resMap.name);
                hero=createHero(map, result);

                if (!body.x || !body.y) return  res.status(404).send('Point if not defined');
                var end = new Vec2(body.x, body.y);
                if(hero.position.isEqual(end)){
                    var mes= hero.name + ' ' + 'is already at x: '+ Math.round(hero.position.x) + '; y: ' + Math.round(hero.position.y);
                    upHistory(mapName, mes);
                    return res.status(200).send(mes);
                }

                if (hero.position.length(end) < hero.rangeMove) {
                    hero.moveTo(end.x, end.y);

                    HeroModel.findByIdAndUpdate(result._id, { $set:{ position: hero.position}},function(err, result){
                        if (err) return next(err);
                    });
                    var mes2=hero.name + ' ' + 'move To x: ' + Math.round(hero.position.x) + '; y: ' + Math.round(hero.position.y);
                    upHistory(mapName, mes2);
                    res.status(200).send(mes2);
                } else {
                    upHistory(mapName, hero.name+' try to move, but Point is further than the character can move');
                    res.status(303).send('Point is further than the character can move, use pattern /hero/addPath');
                }
            });
        });
    });

//======================================================================================================================
 //рух по шляху (hero.way) масив точок

    heroRouter.post('/:mapName/:heroName/moveForWay', function (req, res, next) {
        var mapName= req.params.mapName;
        var heroName=req.params.heroName;
        var hero;

        //знаходжу карту
        MapModel.findOne({name: mapName}).exec(function (err, resMap) {
            if(err)return next(err);
            if(!resMap)return  res.status(404).send('Map is not found, please check the name');

            //знаходжу героя і переверіяю чи він на цій карті
            HeroModel.findOne({ name: heroName},function(err, result){
                if(err) return next(err);
                if(result.currentMap!=resMap.id)return next(err);

                var map=new Map(resMap.name);
                map.updateEnv();
                hero=createHero(map, result);

                if (hero.way!=[]) {
                    hero.moveForWay();
                    HeroModel.findByIdAndUpdate(result._id, { $set:{ position: hero.position, way: hero.way}},function(err, result){
                        if (err) return next(err);
                    });

                    var message = hero.name + ' ' + 'move To x: ' + Math.round(hero.position.x) + '; y: ' + Math.round(hero.position.y)+' '
                        +((hero.way.length)?('He\`s way: '+hero.way):'there is no way, add one if you want;)');
                    //console.log(message);
                    upHistory(mapName, message);
                     res.status(200).send(message);

                } else {
                    upHistory(mapName, hero.name+ 'Try to move for way,but  there is no ways for hero');
                    res.status(303).send('There is no ways for hero, use pattern /hero/addPath to add point to the way');
                }

            });
        });
    });
//======================================================================================================================
//додати точку в кінець шляху

    heroRouter.post('/:mapName/:heroName/addPath', function (req, res, next) {
        var body= req.body;
        var mapName= req.params.mapName;
        var heroName=req.params.heroName;
        //знаходжу карту
        MapModel.findOne({name: mapName}).exec(function (err, resMap) {
            if (err )return next(err);
            if(!resMap)return next(new Error(400, 'Map is not defined'));

            //знаходжу героя і переверіяю чи він на цій карті
            HeroModel.findOne({name: heroName}, function (err, resHero) {
                if (err) return next(err);
                if(!resHero.currentMap) return next(new Error(400, 'Hero is not defined'));
                if(resHero.currentMap != resMap.id) return next(new Error(400, 'Hero on another maps'));


                if (body.x && body.y) {
                    var end = new Vec2(body.x, body.y);
                    HeroModel.findByIdAndUpdate(resHero._id, {$addToSet:{ way: end}},function(err, rez){
                        if (err) return next(err);
                        var mes='Point' + end + ' added as a last point of the way for ' + heroName;
                        upHistory(mapName, mes);
                       return res.status(200).send(mes);
                    });
                } else {
                    next(new Error(400, 'Point is not defined'));
                }

            });
        });
    });

 //======================================================================================================================
//додати шлях

    heroRouter.post('/:mapName/:heroName/addWay', function (req, res, next) {
        var body= req.body;
        var mapName= req.params.mapName;
        var heroName=req.params.heroName;
        //знаходжу карту
        MapModel.findOne({name: mapName}).exec(function (err, resMap) {
            if (err )return next(err);
            if(!resMap)return next(new Error(400, 'Map is not defined'));

            //знаходжу героя і переверіяю чи він на цій карті
            HeroModel.findOne({name: heroName}, function (err, resHero) {
                if (err) return next(err);
                if(!resHero.currentMap) return next(new Error(400, 'Hero is not defined'));
                if(resHero.currentMap != resMap.id) return next(new Error(400, 'Hero on another maps'));


                if (!way) {
                    var way=body.way;
                    HeroModel.findByIdAndUpdate(resHero._id, {$set:{ way: way}},function(err, rez){
                        if (err) return next(err);
                        var mes= 'Way' + rez.way + ' set for' + heroName;
                        upHistory(mapName, mes);
                        return res.status(200).send(mes);
                    });
                } else {
                    next(new Error(400, 'Way is not defined, something wrong'));
                }

            });
        });
    });
//======================================================================================================================
    //метод fight

    heroRouter.post('/:mapName/:heroName/fight/:enemyName', function (req, res, next) {
        var mapName= req.params.mapName;
        var heroName=req.params.heroName;
        var enemyName=req.params.enemyName;
        //знаходимо карту
        MapModel.findOne({name: mapName}).exec(function (err, resMap) {
            if(err)return next(err);

            //знаходимо героя
            HeroModel.findOne({ name: heroName},function(err, resHero){
                if(err) return next(err);

                //знаходимо мішень
                HeroModel.findOne({ name: enemyName},function(err, resEnemy){
                    if(err) return next(err);
                    if(!resMap || !resHero || !resEnemy)return  res.status(400).send('Some objects is not defined');

                    var map=new Map(resMap.name);
                    var enemy=createHero(map, resEnemy);
                    var hero=createHero(map, resHero);
                    if(resMap.id != resEnemy.currentMap || resMap.id != resHero.currentMap){
                        res.status(404).send('Enemy and hero on different maps');
                    }
                    if (enemy.hp > 0) {
                        if (hero.type === enemy.type)return res.status(200).send(enemy.name + ' from your team, he`s not enemy');
                        if (!hero.canAttack(enemy))return res.status(200).send(hero.name + " too far from  " + enemy.name);

                        hero.fight(enemy);
                        HeroModel.findByIdAndUpdate(resEnemy._id, { $set:{ hp: enemy.hp}},function(err){
                            if (err) return next(err);
                        });
                        HeroModel.findByIdAndUpdate(resHero._id, { $set:{ hp: hero.hp}},function(err){
                            if (err) return next(err);
                        });
                        var mes = hero.name + " attack the " + enemy.name;
                        upHistory(mapName, mes);
                        res.status(200).send(mes);

                    } else {
                        res.status(200).send(enemy.name + " is already dead!");
                    }
                });
            });
        });
    });

    return heroRouter;
};