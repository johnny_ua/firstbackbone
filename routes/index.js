
module.exports = function (app, db) {

    var heroRouter = require('./hero.js')(db);
    var mapRouter = require('./map.js')(db);
    var otherRouter = require('./other.js')(db);

    app.all('/', function (req, res, next) {
        console.log('Some request has detected');
        next();
    });

    app.use('/hero', heroRouter);
    app.use('/', otherRouter);
    app.use('/map', mapRouter);


};