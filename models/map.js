/**
 * Created by Ivan on 17.05.2015.
 */
var mongoose = require('mongoose');

var ObjectId = mongoose.Schema.Types.ObjectId;
module.exports = (function () {
    var Schema = mongoose.Schema;
    var LogSchema= new Schema({
        createdAt: {type: Date, default: Date.now()},
        log: {type: String, default: "Maps created"}
    },{versionKey: false, _id: false});



   var MapSchema = new Schema({
        name: {type: String, required: true, unique:[true, 'Name of map must be unique']},

        maxWidth: {type: Number, default: 800},
        maxHeight: {type: Number, default: 500},

        maxWind:{type: Number, default: 5},
        resistance: {type: Number, default: 0},
        gravity: {type: Number, default: 3},

        hasObstacles: {type: Boolean, default: false},
        hasSnow: {type: Boolean, default: false},
        hasWater: {type: Boolean, default: false},
        hasHill: {type: Boolean, default: false},
        hasPit: {type: Boolean, default: false},

        wind: {
                x:{type: Number, default: 1},
                y:{type: Number, default: 1}
        },

        minHeroes: {type: Number},
        maxHeroes: {type: Number},
        heroes: [{ type : ObjectId, ref: 'hero',default: null }],

        about:{type:String, default: "Sorry, this map has no more information now"},
        author: {type: String, default: "Thinkmobiles"},
       updatedAt:{type: Date, default: Date.now()},
        history:[LogSchema]
    },{collection: 'Maps', versionKey: false });

    LogSchema.pre('update', function() {
        this.update({ $set: { createdAt: Date.now() } });
    });

    MapSchema.pre('update', function() {
        this.update({ $set: { updatedAt: Date.now() } });
    });
    MapSchema.virtual('description').get(function () {
        var descr =
            "Name: " + this.name+'\n'
            +"Active Players: " + this.heroes.length+'\n'
            + "Max Width: " + this.maxWidth+'\n'
            + "Max Height: " + this.maxHeight+'\n'
            + "Max Wind: " + this.maxWind+'\n'
            + "Resistance: " + this.resistance+'\n'
            + "Gravity: " + this.gravity+'\n'
            + "Has Obstacles: " + this.hasObstacles+'\n'
            + "Has Snow: " + this.hasSnow+'\n'
            + "Has Water: " + this.hasWater+'\n'
            + "Has Hill: " + this.hasHill+'\n'
            + "Has Pit: " + this.hasPit+'\n'
            +"Created by awesome: " + this.author+'\n'
            +"About: " + this.about;
        return descr;
    });

    if(!mongoose.Schemas){
        mongoose.Schemas={};
    }
    mongoose.Schemas['Map']= MapSchema;
})();