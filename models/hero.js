/**
 * Created by Ivan on 17.05.2015.
 */
var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;
module.exports = (function () {
    var Schema = mongoose.Schema;
    var PointSchema= new Schema({
        x: Number,
        y: Number
    },{versionKey: false, _id: false});

    var HeroSchema = new Schema({
        name: {type: String, required: true, unique:[true, 'Name of hero must be unique']},
        type: {type: String, required: true, enum: ['good', 'bad']},
        clan: {type: String, required: true, enum: ['red', 'shadow']},

        hp: {type: Number, default: 1000, min:100, max: 5000},
        pw: {type: Number, default: 1000, min:10, max: 1000},

        hasCounterAttack: {type: Boolean, default: false},
        speed: {type: Number, default: 3},
        position:{
            x:{type: Number, default: 0},
            y:{type: Number, default: 0}
        },
        rangeMove: {type: Number, default: 10, min:5, max: 200},
        rangeStrike: {type: Number, default: 20, min:5, max: 200},
        way: [PointSchema],
        /*[{
            x:{type: Number},
            y:{type: Number}
        }],*/
        currentMap:{ type : ObjectId, ref: 'map', default: null }
    },{collection: 'Heroes', versionKey: false});


    HeroSchema.virtual('description').get(function () {
        var description=
            "Name: " + this.name+'\n'
            + "Type: " + this.type+'\n'
            + "Clan: " + this.clan+'\n'
            + "Health: " + this.hp+'\n'
            + "Strength: " + this.pw+'\n'
            + "Speed: " + this.speed+'\n'
            + "Attack distance: " + this.rangeStrike+'\n'
            + "Move distance: " + this.rangeMove+'\n';

        return description;
    });


    if(!mongoose.Schemas){
        mongoose.Schemas={};
    }
    mongoose.Schemas['Hero']=HeroSchema;
})();