//----------------------------------------VECTOR---------------------------------------------
//Клас для роботи з двомірними векторами
Vec2= function(x,y) {
	this.x=x;
	this.y=y;
    return this;
};

//Перевизначимо метод toString()
Vec2.prototype.toString= function(){
	return "(" + this.x +"; " + this.y + ")"
}

//Додавання векторів + обмеження на невідємність
Vec2.prototype.add = function(vec) {

    if(this.x + vec.x>0) this.x += vec.x;
	if(this.y + vec.y>0) this.y += vec.y;
	return this;
		
};

//Множення на скаляр
Vec2.prototype.multScalar = function(scalar) {
	this.x *= scalar;
	this.y *= scalar;
	return this;
};



//Нормалізація вектору
Vec2.prototype.normalize = function() {
	if(arguments.length==1 && (arguments[0] instanceof Vec2))
	{	var vec=arguments[0];
		var X = (vec.x - this.position.x);
		var Y = (vec.y - this.position.y);
		var distance = Math.sqrt(X * X + Y * Y);
		this.x=X/distance;
		this.y=Y/distance;
	}
	else {
		var len= this.length(arguments[0]);
		this.x /= len;
		this.y /= len;
	}
	return this;

};

// Клонує обєкт
Vec2.prototype.copy = function (point) {
	var newPoint = {};
	for (var k in point) {
		newPoint[k] = point[k];
	}
	return newPoint;
}

Vec2.prototype.isEqual = function (x,y) {
	if(arguments.length==1 && (arguments[0] instanceof Vec2))
	{	var vec=arguments[0];
		if(this.x=== vec.x && this.y===vec.y)return true;
		return false;
	}
	else {
		if (this.x === x && this.y === y)return true;
		return false;
	}
};

//Довжина відрізка 
Vec2.prototype.length = function(point) {
    if(point===undefined) return Math.sqrt(Math.pow(this.x, 2)+Math.pow(this.y, 2));
    return Math.sqrt(Math.pow(this.x - point.x, 2)+Math.pow(this.y - point.y, 2));
};

//Визначає чи точка point  належить відрізку [start; end] //використаємо рівняння прямої що проходить через дві точки
Vec2.prototype.isBelongsToInterval = function(start, end) {
	if((this.x-start.x)/(end.x-start.x)==(this.y-start.y)/(end.y-start.y) 
		&& (this.x>start.x) && (this.x<end.x) && (this.y>start.y) && (this.y<end.y)) return true;
		return false;
}
module.exports=Vec2;