var Vec2=require("./vec2");

//Функція для створення рандомного числа

var Map = function(name, maxWidth, maxHeight, maxWind, resistance, gravity, hasObstacles, hasSnow, hasWater, hasHill, hasPit ){
	this.name = name || 'NonameMap'+(Math.random()*100).toFixed(0);
	this.maxWidth = maxWidth || 800;			// розміри
	this.maxHeight = maxHeight || 500;
	this.maxWind = maxWind || 3; 				// максимальна сила вітер для мапи
	this.resistance = resistance || 0; 			// опір середовища
	this.gravity = gravity || 0;				// гравітація або якась інша сила
	this.hasObstacles = hasObstacles || false; 	// наявність перешкод
	this.hasSnow = hasSnow || false; 			// наявність снігу
	this.hasWater = hasWater || false; 			// наявність води
	this.hasHill = hasHill || false; 			// наявність гір
	this.hasPit = hasPit || false;				// наявність обривів (урвищ, ям -незнаю як правильно)
	this.wind= new Vec2(0,0);
	this.minHeroes=2;
	this.maxHeroes=20;

	this.heroes=[];								// масив гравців (точноше персонажів на карті)
	this.ways=[];								// масив шляхів

	this.about = "Sorry, this map has no more information now";
	this.author = "Thinkmobiles";
};

//Розробник може ввести додаткову інформацію про карту, або способи її проходження і тд...
Map.prototype.setInfo = function(strValue){
	 this.about=strValue;
};



Map.prototype.printAllHeroes = function (){
	var heroes= this.heroes;
            console.log('=====================================');
             for (var i = 0,l = heroes.length; i < l; i++) {
                if (!heroes[i].isAlive()) console.log(heroes[i].name+' '+heroes[i].type+' --- ' + 'dead');
                 else console.log(heroes[i].name+' '+heroes[i].type+' --- '+heroes[i].hp +'hp  '+heroes[i].pw +'pw' );
          }
          console.log('=====================================');
        };

//Зміна властивостей середовища
Map.prototype.updateEnv = function(){
	// тут можуть змінюватися парметри 
	this.wind = new Vec2(Math.round(Math.random() * (2*this.maxWind))-this.maxWind,
		Math.round(Math.random() * (2*this.maxWind))-this.maxWind);
};


//Перевірка умови завершення гри
Map.prototype.gameOver= function (){
	var countGood=0;
	var	countBad=0;
	var heroes=this.heroes;

	for (var i = heroes.length; i-- ;) {
		if(heroes[i].type==='good'&& heroes[i].hp>0)countGood++;
		else if(heroes[i].type==='bad'&& heroes[i].hp>0)countBad++;
		//ToDo узагальнити для довільних типів (кланів, команд) героїв
	}
	if(countGood === 0 && countBad !== 0) return 'wins the BAD team \n    !!!GAME OVER!!!';
	if(countBad === 0 && countGood !== 0) return 'wins the GOOD team \n    !!!GAME OVER!!!';
	if(countBad === 0 && countGood === 0) return 'nobody wins \n    !!!GAME OVER!!!';
	return undefined;
};

Map.prototype.autoPlay= function (){
	var heroes=this.heroes;
	for (var i = heroes.length; i-- ;) {
		if(heroes[i].isAlive() && !this.gameOver()){
			heroes[i].moveForWay().fightAll();
		}
	}
};
Map.prototype.timer = function  (seconds, someFunc)
{
	var count=0, now, start = new Date();
	while (true)
	{
		now = Date.now();
		if ((now - start) >= 1000)
		{
			start=now;
			//console.log(count+1);
			now=Date.now();
			count++;
			if (count==seconds)
			{
				someFunc();
				return
			}
		}
	}
};
// Отримати опис мапи
Map.prototype.description = function (){
	var descr =
		"Name: " + this.name+'\n'
//+ "Active Players: " + this.heroes.length+'\n'
		+ "Max Width: " + this.maxWidth+'\n'
		+ "Max Height: " + this.maxHeight+'\n'
		+ "Max Wind: " + this.maxWind+'\n'
		+ "Resistance: " + this.resistance+'\n'
		+ "Gravity: " + this.gravity+'\n'
		+ "Has Obstacles: " + this.hasObstacles+'\n'
		+ "Has Snow: " + this.hasSnow+'\n'
		+ "Has Water: " + this.hasWater+'\n'
		+ "Has Hill: " + this.hasHill+'\n'
		+ "Has Pit: " + this.hasPit+'\n'
		+"Created by awesome: " + this.author+'\n'
		+"About: " + this.about;
	return descr;
};

module.exports=Map;