/**
 * Created by Ivan on 24.04.2015.
 */
var Vec2 = require('./vec2');
var way1=[
    new  Vec2(70, 70),
    new  Vec2(100,150),
    new  Vec2(300,210),
    new  Vec2(390,240)
];
var way2=[
    new  Vec2(10, 50),
    new  Vec2(50, 100),
    new  Vec2(200,150),
    new  Vec2(320,210),
    new  Vec2(395,255)
];
var way3=[
    new  Vec2(750,450),
    new  Vec2(500,400),
    new  Vec2(480,350),
    new  Vec2(410,260)
];
var way4=[
    new  Vec2(700,480),
    new  Vec2(500,450),
    new  Vec2(490,310),
    new  Vec2(410,245)
];

//way3.reverse();
//way4.reverse();
module.exports= function (num) {
	switch(num) {
		case 1: return way1;
		case 2: return way2;
		case 3: return way3;
		case 4: return way4;
			default: return [];
	}
};


