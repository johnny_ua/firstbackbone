var ShadowDragon = require('./shadowDragon');

//---------------------------------RED DRAGON-----------------------------------------------
// Клас персонажа нащадка
var RedDragon = function(currentMap, name, type, health, power) {
    ShadowDragon.apply(this, arguments);
    this.type = type || 'good';
    this.clan = 'red';
    this.hp = health || this.hp*2;
    this.pw = power || this.pw *2;
    this.rangeStrike *=2;
    this.rangeMove *=2;
    this.hasCounterAttack=true;

};

RedDragon.prototype=Object.create(ShadowDragon.prototype);
RedDragon.prototype.constructor=RedDragon;

module.exports=RedDragon;