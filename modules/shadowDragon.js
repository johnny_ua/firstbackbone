var Vec2 = require('./vec2');
//======================================================================================================================
//------------------------SHADOW DRAGON----------------------------------------------------------
//Клас персонажа предка
ShadowDragon = function(currentMap ,name, type, health, power, position, way) {
	//this.id=currentMap.heroes.length||0;
    this.name = name;
    this.type = type || 'bad';
    this.clan = 'shadow';
    this.hp = health || 1000;               // Здоров'я
    this.pw = power || 100;                 // Сила удару
    this.hasCounterAttack=false;               //Можливість контратаки

    //Розміщення хороших і поганих в протилежних краях карти
    this.position = (position)?new Vec2(position.x, position.y):this.type=='bad'? new Vec2(1,1): new Vec2(currentMap.maxWidth-1, currentMap.maxHeight-1);
    this.speed = 10;                        //Швидкість
    this.rangeMove= 40;                     // Дальність переміщення
    this.rangeStrike = 30;                  // Радіус удару
    this.currentMap=currentMap;
    //Заносимо обєкт до масиву всіх персонажів 
    currentMap.heroes.push(this);
    //this.position = (position)?new Vec2(position.x, position.y):new Vec2(1,1);
    this.way = way || [];
}; 

//РОЗШИРЮЄМО КЛАС
ShadowDragon.prototype.userControl = function () {
     return pseudoUserControl();
};
ShadowDragon.prototype.setWay = function (way) {
    this.way=way;
};
ShadowDragon.prototype.hasWay = function () {
    if(this.way.length===0)return false;
    return true;
};

//Оновлення розміщення персонажу (рух)
ShadowDragon.prototype.moveTo = function(x,y) {
    var maxX=800;//this.currentMap.maxWidth;
    var maxY=500;//this.currentMap.maxHeight;

    if(0<x && x<maxX)  this.position.x=x;
            else if (this.position.x < 0) this.position.x = 0;
                else  this.position.x = maxX;

    if(0<y && y<maxY)this.position.y=y;
        else if (this.position.y < 0) this.position.y = 0;
             else this.position.y = maxY;

    console.log('>>>'+this.name+' '+ 'move To x: '+ Math.round(this.position.x)+ '; y: '+ Math.round(this.position.y));
    return this; //повертаємо this  для можливості виклику в ланцюжку
};


ShadowDragon.prototype.moveForWay = function() {
    var path = this.way.shift();
    if (path === undefined) {
        console.log('>>>' + this.name + ' waiting on ' + this.position);
        return this;
    }
    var x, y,cx,cy, angle, teta, windNorm, vec,distance, needToMoveX, needToMoveY, needToMoveDistance;

   // Вплив вітру, враховуючи силу і кут відносно руху персонажа
    //--------------------------------------------------------------------
    if (!this.currentMap.wind.isEqual(0, 0)){
        windNorm = new Vec2(0, 0).copy(this.currentMap.wind).normalize();
         cx=this.position.x; //current x
         cy=this.position.y; //current y
        x=(cx<path.x)?path.x-cx:cx-path.x;
        y=(cy<path.y)?path.y-cy:cy-path.y;
        vec= new Vec2(x, y).normalize();
        teta = Math.acos(windNorm.x * vec.x + windNorm.y * vec.y);
        distance = Math.round(this.rangeMove+Math.cos(teta).toFixed(5)*this.currentMap.wind.length());
    } else{
        distance = this.rangeMove;
    }
    //------------------------------------------------------------------
    while(!!distance){
        needToMoveX = Math.abs(path.x - this.position.x);
        needToMoveY = Math.abs(path.y - this.position.y);
        needToMoveDistance = Math.sqrt(needToMoveX * needToMoveX + needToMoveY * needToMoveY);
        angle = Math.atan(needToMoveY/needToMoveX);     //кут у радіанах між віссю Оx та напрямком руху героя// grad=rad*(180/Math.PI);

        if(path===undefined)return this;    // перевіряємо чи існує наступна точка маршруту
        if (needToMoveDistance < distance){      // якщо відрізок менший за дальність героя
            this.moveTo(path.x, path.y);         // то герой рухається до його кінця
           distance -= needToMoveDistance;     // дальність переміщення героя зменшується
            console.log(this.name+' reached  point : ' + path);
            path=this.way.shift();              // і беремо наступний відрізок
            if (path === undefined){console.log(this.name+' reached the last point of the way');return this;}
       } else {
          if(path.x<this.position.x) x = Math.round(this.position.x - distance * Math.cos(angle));
                                 else x =Math.round(this.position.x + distance * Math.cos(angle));
           if(path.y<this.position.y) y =Math.round(this.position.y - distance * Math.sin(angle));
                                 else y =Math.round(this.position.y + distance * Math.sin(angle));

            this.moveTo(x,y);                   //інакше проходимо по відрізку на дальність героя
            this.way.unshift(path);             //повертаємо назад відрізок, адже він ще не пройдений
            return this;
        }
    }
    return this;

};

ShadowDragon.prototype.getSpeed = function(){
   return world.wind
    .add(userControl)
    .multScalar(world.airResistance);
    // return this;
};

//Перевірка чи персонаж ворожий і чи достатньо близько для удару 
ShadowDragon.prototype.canAttack = function (enemy){
        return !!(this.position.length(enemy.position) <= this.rangeStrike && this.type !== enemy.type);
};

//Власне сам удар
ShadowDragon.prototype.fight = function(enemy) {

      if(this.canAttack(enemy) && this.isAlive() && enemy.isAlive()) { 
        enemy.hp-=this.pw;

        console.log(this.type+' '+this.name+' attack '+ enemy.type+' '+ enemy.name);

        //Дивимось чи персонаж живий
        if(!enemy.isAlive()) console.log('+++'+ this.name+' kill the '+ enemy.name);
        
        //Контр атака якщо присутній такий атрибут і  ворог достатньо близько для удару
        else if(enemy.hasCounterAttack) enemy.counterAttack(this);
      }
};

// Удар по всіх персонажах які в радіусі удару
ShadowDragon.prototype.fightAll = function() {
    for (var i = this.currentMap.heroes.length; i-- ;) {
        this.fight(this.currentMap.heroes[i])
      }
};

//Окремий метод для контр атаки щоб уникнути зациклення коли два персонажі контр атакуватимуть до смерті 
ShadowDragon.prototype.counterAttack = function(enemy) {
      if(this.canAttack(enemy) && this.isAlive() && enemy.isAlive()) { 
        enemy.hp-=this.pw;

        console.log(this.type+' '+this.name+' counter attack '+ enemy.type+' '+ enemy.name);

        //Дивимось чи персонаж живий
        if(!enemy.isAlive()){ 
            console.log('+++'+ this.name+' kill the '+ enemy.name);
            
        }
      }
};

//Чи живий персонаж
ShadowDragon.prototype.isAlive = function (){
    if(this.hp>0) return true;
    this.hp=0;
    return false;
};

ShadowDragon.prototype.description = function(){
var description=
         'Name: ' + this.name+'\n'
         + "Type: " + this.type+'\n'
         + "Clan: " + this.clan+'\n'
         + "Health: " + this.hp+'\n'
         + "Strength: " + this.pw+'\n'
         + "Speed: " + this.speed+'\n'
         + "Attack distance: " + this.rangeStrike+'\n'
         + "Move distance: " + this.rangeMove+'\n';


    return description;
};


module.exports=ShadowDragon;