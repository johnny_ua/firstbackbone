
define([
    'Backbone',
    'views/mainView',
    'views/canvasView',
    'views/hero/content'
], function (Backbone, mainView, canvasView, heroView) {
    var Router = Backbone.Router.extend({

        mainView: null,
        contentView: null,

        routes: {
            "home": "home",
            "game/:Content": "getContent",
            '*any': 'home'
        },

        getContent: function (Content) {
            var contentViewUrl = 'views/' + Content + '/content';
            var contentCollection = 'collections/' + Content.toLowerCase() + 'Collection';
            var self = this;

            require([contentCollection, contentViewUrl], function(Collection, contentView){
                var collection = new Collection();

                var f = self.changeContentView.bind(self, contentView, collection);

                collection.on('reset', f, self);
            });
        },

        changeContentView: function(view, collection){
            if(this.contentView){
                this.contentView.undelegateEvents();
            }
            this.contentView = new view({collection: collection});
        },

        home: function () {
            if (this.mainView) {
                this.mainView.undelegateEvents();
            }
            this.mainView = new mainView();
        }
    });

    return Router;
});