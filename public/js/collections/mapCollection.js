/**
 * Created by Ivan on 14.06.2015.
 */
define([
    'models/mapModel'
], function (mapModel) {
    var Collection = Backbone.Collection.extend({
        model: mapModel,
        initialize: function(){
            this.fetch({
                reset: true
            });
        },
        url: '/map'
    });

    return Collection;
});
