/**
 * Created by Ivan on 14.06.2015.
 */
define([
    'models/heroModel'
], function (heroModel) {
    var Collection = Backbone.Collection.extend({
        model: heroModel,
        initialize: function(){
            this.fetch({
                reset: true
            });
        },
        url: function () {
            return "/hero"
        }
    });

    return Collection;
});
