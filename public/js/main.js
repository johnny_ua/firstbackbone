/**
 * Created by Roman on 25.05.2015.
 */
var App = App ||  {  };

require.config({
    paths: {
        jQuery: './libs/jquery/dist/jquery.min',
        jQueryUI: './libs/jqueryui/jquery-ui.min',
        Underscore: './libs/underscore/underscore-min',
        Backbone: './libs/backbone/backbone-min',
        less: './libs/less/dist/less.min',
        templates: '../templates', // templates dir not error
        text: './libs/text/text',
        views: './views',
        models: './models',
        collections: './collections'

    },
    shim: {
        'Bootstrap': ['jQuery'],
        'Backbone': ['Underscore', 'jQuery'],
        'jQueryUI':['jQuery'],
        'app': ['Backbone', 'less','jQueryUI']
    }
});

require(['app'], function(app){
    app.init();
});