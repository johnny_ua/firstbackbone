define([
    'text!templates/topBarTemplate.html',
    'views/map/createMapView',
    'views/hero/createHeroView'
], function (topBarTemplate, mapModalView) {

    var View;
    View = Backbone.View.extend({
        el: '#topBar',

        events: {
            "click .showModalMap":"showModalMap",
            "click .ui-dialog-titlebar-close":"closeDialog",
            "click .maps": "getView",
            "click .heroes": "getView",
            "click .playGame": "getCanvasView",
            "click #logo": "getCanvasView"
        },

        template: _.template(topBarTemplate),


        initialize: function () {
            // keep menu actual
            this.render();
        },

        getView : function (e) {
            e.preventDefault();
            e.stopPropagation();

            var targetEl = $(e.target);
            var hash = targetEl.data('hash');
            hash = 'game/' + hash;

            Backbone.history.navigate(hash, {trigger: true});
        },
        getCanvasView: function (e) {
            e.preventDefault();
            e.stopPropagation();
            Backbone.history.navigate('#home', {trigger: true});
        },

        closeDialog:function(e){
            e.preventDefault();
            $(".ui-dialog").remove();
        },
        showModalMap:function(){
            var MapModalView  = new mapModalView();
        },



        changeTab: function(event) {
            var holder = $(event.target);
            var closestEl = holder.closest('.loggedMenu');
            closestEl.find(".active").removeClass("active");
            holder.addClass("active");

        },


        render: function () {
            //$(#content).html();
            this.$el.html(this.template());

            return this;
        }
    });
    return View;
});
