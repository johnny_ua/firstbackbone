/**
 * Created by Ivan on 14.06.2015.
 */
define([
    'text!templates/map/createMapModalTemplate.html',
    "models/mapModel"
], function ( modalTemplate, Model) {

    var View;

    View = Backbone.View.extend({
        el:"#topBar",
        events: {
            "click .newMap": "createMap"
        },


        initialize: function () {
            this.render();
        },

        createMap: function(e){
            e.stopPropagation();
            var mapModel = new Model();
            mapModel.save({
                    name : this.$el.find("#name").val(),
                    maxWidth : this.$el.find("#width").val(),
                    maxHeight : this.$el.find("#height").val(),
                    wind:{
                        x : this.$el.find("#windX").val(),
                        y : this.$el.find("#windY").val()
                    },
                    author : this.$el.find("#author").val(),
                    about : this.$el.find("#about").val()
                },
                {
                    wait: true,
                    success: function (model, response) {
                        alert("OK!");
                        e.preventDefault();
                        $(".ui-dialog").remove();
                        window.location="/#home";
                    },
                    error: function (model, xhr) {
                        this.errorNotification(xhr);
                        e.preventDefault();
                        $(".ui-dialog").remove();
                         window.location="/#home";
                    }
                });
        },
        // render template (once! because google maps)
        render: function () {
            Backbone.history.navigate('#home', {trigger: true});
            var formString = _.template(modalTemplate)({
            });
            this.dialog = $(formString).dialog({
                modal:true,
                closeOnEscape: false,
                appendTo:"#topBar",
                dialogClass: "edit-dialog",
                width: 700,
                resizable: false
            });
            return this;
        }


    });
    return View;
});
