

define([
    'text!templates/map/mapsTemplate.html',
    'collections/mapCollection',
    'views/hero/createHeroView',
    'models/heroModel'
], function (heroTemplate, mapCollection, heroModalView) {
    var contentView = Backbone.View.extend({
        el: '#contentHolder',
        template: _.template(heroTemplate),

        events: {
            //'click .remove': 'removeMap',
            "click .add":"showModalHero",
            "click .newHero": "createHero"
        },

        initialize: function (opt) {
            this.collection = opt.collection;
            this.render();
        },
        createHero: function(e){
            e.stopPropagation();
            var heroModel = new heroModel();
            var id = $(e.target).attr('id');
            heroModel.save({
                    //currentMap: id,
                    name : this.$el.find("#name").val(),
                    type : this.$el.find("#type").val(),
                    clan : this.$el.find("#clan").val(),
                    position:{
                        x : this.$el.find("#posX").val(),
                        y : this.$el.find("#posY").val()
                    },
                    speed : this.$el.find("#speed").val(),
                    rangeMove : this.$el.find("#rangeMove").val(),
                    rangeStrike : this.$el.find("#rangeStrike").val()
                },
                {
                    wait: true,
                    success: function (model, response) {
                        //mapCollection.add(model);
                        alert("OK!");
                        e.preventDefault();
                        $(".ui-dialog").remove();
                        window.location="/#home";
                    },
                    error: function (model, xhr) {
                        console.log(xhr);
                        e.preventDefault();
                        $(".ui-dialog").remove();
                        window.location="/#home";
                    }
                });
        },
        showModalHero: function (e) {
            var HeroModalView  = new heroModalView();
        },
        removeMap: function (e) {
            var id = $(e.target).attr('id');
            var model = this.collection.get(id);

            model.destroy({
                success: function (model) {
                    Backbone.history.fragment = '';
                    Backbone.history.navigate('game/map', {trigger: true});
                },
                error: function (err, xhr, model) {
                    alert(xhr);
                }
            });
        },

        render: function () {
            var collection = this.collection.toJSON();
            this.$el.html(this.template({collection: collection}));
            return this;
        }
    });

    return contentView;
});