/**
 * Created by Roman on 28.05.2015.
 */
define([
    'text!templates/mainViewTemplate.html',
    'views/topBarView',
    'views/canvasView',
    'views/hero/content'
], function (mainTemplate, topBarView, canvasView, heroesView) {
    var mainView = Backbone.View.extend({
        el: '#content',

        template: _.template(mainTemplate),

        initialize: function () {
            //this.collection = opt.collection;
            this.view = canvasView;
            this.render();
        },

        render: function () {
            //$(#content).html();
            this.$el.html(this.template());

            var topBar = new topBarView();
            var canvas = new this.view();

            return this;
        }
    });

    return mainView;
});