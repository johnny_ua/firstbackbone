/**
 * Created by Ivan on 14.06.2015.
 */
define([
    'text!templates/hero/createHeroModalTemplate.html',
    'collections/mapCollection',
    'models/heroModel'

], function ( modalTemplate, mapCollection, heroModel) {

    var View;

    View = Backbone.View.extend({
        el:"#topBar",
        events:{},
        initialize: function () {
            this.render();
        },

        click: function () {
          alert('sdfvgfdv');
        },
        newHero: function(e){
            //e.stopPropagation();
            var heroModel = new heroModel();
            //var id = $(e.target).attr('id');
            heroModel.save({
                    //currentMap: id,
                    name : this.$el.find("#name").val(),
                    type : this.$el.find("#type").val(),
                    clan : this.$el.find("#clan").val(),
                    position:{
                        x : this.$el.find("#posX").val(),
                        y : this.$el.find("#posY").val()
                    },
                    speed : this.$el.find("#speed").val(),
                    rangeMove : this.$el.find("#rangeMove").val(),
                    rangeStrike : this.$el.find("#rangeStrike").val()
                },
                {
                    wait: true,
                    success: function (model, response) {
                        //mapCollection.add(model);
                        alert("OK!");
                        e.preventDefault();
                        $(".ui-dialog").remove();
                        window.location="/#home";
                    },
                    error: function (model, xhr) {
                       console.log(xhr);
                        e.preventDefault();
                        $(".ui-dialog").remove();
                        window.location="/#home";
                    }
                });
        },
        // render template (once! because google maps)
        render: function () {
            var self=this;
            Backbone.history.navigate('#home', {trigger: true});
            var formString = _.template(modalTemplate)({
            });
            this.dialog = $(formString).dialog({
                modal:true,
                closeOnEscape: false,
                appendTo:"#topBar",
                dialogClass: "edit-dialog",
                width: 700,
                resizable: false
                /* ,buttons: [{
                    text: "create"
                    //click: self.newHero()
                }]
              create:function () {
                    $(this).closest(".ui-dialog")
                        .find(".ui-button:first") // the first button
                        .addClass("newHero");
                }*/
            });

            return this;
        }


    });
    return View;
});
