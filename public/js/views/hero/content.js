

define([
    'text!templates/hero/heroesTemplate.html'
], function (heroTemplate) {
    var contentView = Backbone.View.extend({
        el: '#contentHolder',
            template: _.template(heroTemplate),

            events: {
                'click .remove': 'removeHero'
            },

            initialize: function (opt) {
                this.collection = opt.collection;
                this.render();
            },

        removeHero: function (e) {
                var id = $(e.target).attr('id');
                var model = this.collection.get(id);
                var self = this;
                model.destroy({
                    success: function (model) {
                        self.collection.fetch();
                        Backbone.history.fragment = '';
                        Backbone.history.navigate('game/hero', {trigger: true});
                    },
                    error: function (err, xhr, model) {
                        self.collection.fetch();
                        console.log(xhr);
                        Backbone.history.fragment = '';
                        Backbone.history.navigate('game/hero', {trigger: true});
                    }
                });
            },

            render: function () {
                var collection = this.collection.toJSON();
                //console.log(this.collection.toJSON());
                this.$el.html(this.template({collection: collection}));
                return this;
            }
        });

    return contentView;
});