/**
 * Created by Roman on 28.05.2015.
 */
define([
    'text!templates/canvasTemplate.html',
    'collections/heroCollection'

], function (canvasTemplate, heroCollection) {
    var canvasView = Backbone.View.extend({
        el: "#contentHolder",
        events: {
            //"mousedown": "mousedownHandler", // on #someDIV
            "mousedown #myCanvas": "mouseupHandler",
            "dblclick #myCanvas": "dblclickHandler",
            "click .move": "move"
        },
        getHero: function (mx, my) {
            //height & weight of hero on canvas
            var hw = 50, hh = 50;
            var hpx,
                hpy;
            return _.find(this.myCollection.toJSON(), function(hero){
                hpx = hero.position.x;
                hpy = hero.position.y;
                return (hpx <= (mx + hw / 2) && hpx >= (mx - hw / 2) && hpy <= (my + hh / 2) && hpy >= (my - hh / 2));
            });
           /* this.myCollection.toJSON().forEach(function (hero) {
                //hero position
                hpx = hero.position.x;
                hpy = hero.position.y;

                console.log(hpx <= (mx+hw/2) && hpx>=(mx-hw/2) && hpy<=(my+hh/2) && hpy>=(my-hh/2));
                if (hpx <= (mx + hw / 2) && hpx >= (mx - hw / 2) && hpy <= (my + hh / 2) && hpy >= (my - hh / 2)) {
                    return hero;
                }
            });

            return false;*/
        },
        setHero: function (hero, x, y) {
            var model = this.myCollection.get({_id: hero.id});
            var pos = {
                x: x,
                y: y
            };
            model.save({"position": pos},
                {
                    success: function (model, response) {
                        console.log('success');
                    },
                    error: function () {
                        console.log('error');
                    }
                });
        },

        mouseupHandler: function (e) {
            var m = this.getMousePos(e, this.canvas);
            if (this.getHero(m.x, m.y)) {
                console.log('yeap')
                 this.start = true;
                this.ctx.beginPath();
                this.ctx.setLineDash([2]);
                this.ctx.moveTo(m.x,m.y);
            }
        },

        dblclickHandler: function (e) {
            if(this.start){
                this.draw();
                var m = this.getMousePos(e, this.canvas);
                this.ctx.lineTo(m.x, m.y);
                //this.ctx.closePath();
                this.ctx.stroke();
            }


        },

        mousedownHandler: function (e) {
            //var mx = e.pageX;
            //var my = e.pageY;
            e.preventDefault();
            this.ctx.fillStyle = "#FF9653";
            var m = this.getMousePos(e, this.canvas);
            var start = false;
            if (this.getHero(m.x, m.y)) {
                this.start = true;

            }
            console.log(this.getHero(m.x, m.y));
            this.ctx.fillRect(m.x, m.y, 10, 10);
            this.drawLine(e, this.ctx);


            //alert('Mouse down'+ mouse);
            /*var data={
             x :12,
             y:23

             };

             $.ajax({
             type: "POST",
             url: "/testTrackVideo",
             data: JSON.stringify(data),
             contentType: "application/json",

             success: function (msg) {
             if (msg) {
             console.log('Successfully send')
             } else {
             console.log("Cant track the video");
             }
             },
             error: function (model, xhr) {
             console.log(xhr);
             console.log(model);

             }
             });*/
        },
        getMousePos: function (e, canvas) {
            return {
                x: e.pageX - this.canvas.offsetLeft - 5,
                y: e.pageY - this.canvas.offsetTop - 5
            };
        },
        drawLine: function (e, context) {
            context.beginPath();
            context.setLineDash([5]);
            context.moveTo(300, 300);
            var m = this.getMousePos(e, this.canvas);
            context.lineTo(m.x, m.y);
            context.stroke();
        },
        template: _.template(canvasTemplate),

        initialize: function () {
            this.myCollection = new heroCollection();
            this.listenTo(this.myCollection, "reset", this.render);
            // this.render();
        },
        move: function () {
            var canvas = document.getElementById('myCanvas');
            var context = canvas.getContext('2d');
            runAnimation.value = !runAnimation.value;

                if(runAnimation.value) {
                    var date = new Date();
                    var time = date.getTime();
                    animate(time, myRectangle, runAnimation, canvas, context);
                }

            drawRect(myRectangle, context);
           /* var context = this.ctx;
            context.closePath();
            context.lineWidth = 2;
            context.strokeStyle = 'blue';
            context.stroke();*/
        },
        animate: function (lastTime, myRectangle, runAnimation, canvas, context) {
        if(runAnimation.value) {
            // update
            var time = (new Date()).getTime();
            var timeDiff = time - lastTime;

            // pixels / second
            var linearSpeed = 100;
            var linearDistEachFrame = linearSpeed * timeDiff / 1000;
            var currentX = myRectangle.x;

            if(currentX < canvas.width - myRectangle.width - myRectangle.borderWidth / 2) {
                var newX = currentX + linearDistEachFrame;
                myRectangle.x = newX;
            }

            // clear
            context.clearRect(0, 0, canvas.width, canvas.height);

            // draw
            drawRect(myRectangle, context);

            // request new frame
            requestAnimFrame(function() {
                animate(time, myRectangle, runAnimation, canvas, context);
            });
        }
    },
        draw: function(){
            var ctx = this.ctx;
            ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
            console.log(this.myCollection.toJSON());
            this.myCollection.toJSON().forEach(function (hero) {
                //console.log(hero);
                //ctx.fillRect(hero.position.x, hero.position.y, 10, 10);
                ctx.beginPath();
                ctx.strokeStyle = '#ff0000';
                ctx.arc(hero.position.x,hero.position.y,20,0,2*Math.PI);
                ctx.stroke();
            });
        },
        render: function () {
            this.$el.html(this.template());
            this.canvas = document.getElementById("myCanvas");
            this.ctx = this.canvas.getContext("2d");
            this.draw()
            return this;
        }
    });

    return canvasView;
});

