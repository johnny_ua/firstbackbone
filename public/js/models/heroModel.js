define([], function(){
    var Model = Backbone.Model.extend({
        idAttribute: '_id',
        /*url:  function(){
         return '/user/' + this.get('age') + '/' + this.id
         }*/
        urlRoot: function(){
            return '/hero/'
        }
    });

    return Model;
});