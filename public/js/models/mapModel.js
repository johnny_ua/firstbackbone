/**
 * Created by Ivan on 14.06.2015.
 */
define([], function(){
    var Model = Backbone.Model.extend({
        idAttribute: '_id',
        /*url:  function(){
         return '/user/' + this.get('age') + '/' + this.id
         }*/
        urlRoot: function () {
            return "/map"
        }
    });

    return Model;
});

//GET - fetch //
//POST  Model.save
//PUT model.save
//PATCH
//DELETE